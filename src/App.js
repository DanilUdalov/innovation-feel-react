import React, { Component } from 'react';
import './styles/App.css';
import TokenAuth from './components/TokenAuth.js';
import { CookiesProvider } from 'react-cookie';

class App extends Component {
  render() {
    return (
      <div className="App">
        <CookiesProvider>
          <TokenAuth />
        </CookiesProvider>
      </div>
    );
  }
}

export default App;
