import React, { Component } from 'react'
import { Row, Navbar, Nav, NavItem, Glyphicon, Alert, Col} from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Welcome from './Welcome.js'
import LogIn from './LogIn.js'
import logo from './images/if-logo.png'

class AppHeaderComponent extends Component {
  render() {
    return (
      <div>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          {this.props.appState.auth_token &&
            <h4>{this.props.appState.email} - {this.props.appState.role}</h4>
          }
          {this.props.appState.auth_token && this.props.appState.role == 'admin' &&
            <a href='/admin'>
              <Glyphicon glyph="cog" /> Admin panel
            </a>
          }
        </header>
        <Navbar default collapseOnSelect>
          <Navbar.Header>
            <LinkContainer exact to="/">
              <Navbar.Brand eventKey={1}>
                <Glyphicon glyph="star-empty" />
              </Navbar.Brand>
            </LinkContainer>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              <LinkContainer exact to="/">
                <NavItem eventKey={2}>
                  Home
                </NavItem>
              </LinkContainer>
              <LinkContainer exact to="/articles">
                <NavItem eventKey={3}>
                  Articles
                </NavItem>
              </LinkContainer>
            </Nav>
            <Nav pullRight>
              {!this.props.appState.auth_token &&
                <LinkContainer exact to="/log_in">
                  <NavItem eventKey={4}>
                    Log In
                  </NavItem>
                </LinkContainer>
              }

              {!this.props.appState.auth_token &&
                <LinkContainer exact to="/registration">
                  <NavItem eventKey={5}>
                    Registration
                  </NavItem>
                </LinkContainer>
              }
  
              {this.props.appState.auth_token &&
                <LinkContainer exact to="/sign_out">
                  <NavItem eventKey={6}>
                    Sign Out
                  </NavItem>
                </LinkContainer>
              }
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Row>
          <Col md={4} mdOffset={1} xs={12}>
            {this.props.appState.notification &&
              <Alert bsStyle={this.props.appState.alert_style}>
                <p>{ this.props.appState.notification }</p>
              </Alert>
            }
          </Col>
        </Row>
      </div>
    )
  }

  constructor(props) {
    super(props)
  }

}

export default AppHeaderComponent
