import React, { Component } from 'react'
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel, Button, Alert, Checkbox } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'
import { RingLoader } from 'react-spinners'

const Api = require('../lib/Api.js')

class ForgotPasswordComponent extends Component {
  render() {
    return (
      <div className='ForgotPassword'>
        <Col md={4} mdOffset={3} xs={12}>
          <Row>
            {this.state.errors && this.state.formSubmitted &&
              <Alert bsStyle='danger'>
                <p>{ this.state.errors }</p>
              </Alert>
            }
          </Row>
          <Row>
            {this.state.loading == true &&
              <center>
                <div className='sweet-loading'>
                  <RingLoader
                    color={'#ff005c'}
                    loading={ this.state.loading }
                  />
                </div>
              </center>
            }
          </Row>
          <form onSubmit={ this.handleSubmit }>
            <Row>
              <FormGroup>
                <ControlLabel>Enter your email</ControlLabel>
                <FormControl
                  id='email'
                  type='email'
                  label='Email'
                  name='email'
                  placeholder='Enter email'
                  onChange={ this.setInputValue }
                  required true
                />
              </FormGroup>
            </Row>
            <Row>
              <Button type='submit' className='btn btn-warning'>Reset password</Button>
            </Row>
          </form>
        </Col>
      </div>
    )
  }

  defaultState() {
    return {
      email: null,
      errors: null,
      loading: false,
      formSubmitted: false
    }
  }

  constructor(props) {
    super(props)

    this.state = this.defaultState()
    this.handleSubmit = this.handleSubmit.bind(this)
    this.setInputValue = this.setInputValue.bind(this)
  }

  setInputValue(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  redirectToMainPage() {
    this.props.history.push('/');
  }

  handleSubmit(event) {
    event.preventDefault()
    this.setState({
      formSubmitted: true,
      errors: null,
      loading: true
    })

    Api.forgotPassword(this.state.email).then(message => {
      if (typeof message.error === 'undefined') {
        alert(message);
        this.redirectToMainPage();
      }
      else {
        this.setState({
          errors: message.error,
          loading: false
        })
      }
    })
  } 
}

export default withRouter(ForgotPasswordComponent);
