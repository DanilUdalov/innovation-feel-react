import React, { Component } from 'react';
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel, Button, Alert, Checkbox } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { withRouter } from 'react-router-dom'
import { RingLoader } from 'react-spinners'

const Api = require('../lib/Api.js')

class LogInComponent extends Component {
  render() {
    return (
      <div className='Signin'>
        <Col md={4} mdOffset={3} xs={12}>
          <Row>
            {this.state.errors && this.state.formSubmitted &&
              <Alert bsStyle='danger'>
                <p>{ this.state.errors }</p>
              </Alert>
            }
          </Row>
          <Row>
            {this.state.loading == true &&
              <center>
                <div className='sweet-loading'>
                  <RingLoader
                    color={'#ff005c'}
                    loading={ this.state.loading }
                  />
                </div>
              </center>
            }
          </Row>
          <form onSubmit={ this.handleSubmit }>
            <Row>
              <FormGroup>
                <ControlLabel>Email</ControlLabel>
                <FormControl
                  id='email'
                  type='email'
                  label='Email'
                  name='email'
                  placeholder='Enter email'
                  onChange={ this.setInputValue }
                  required true
                />
              </FormGroup>
            </Row>
            <Row>
              <FormGroup>
                <ControlLabel>Password</ControlLabel>
                <FormControl
                  id='password'
                  type='password'
                  label='Password'
                  name='password'
                  placeholder='Enter password'
                  onChange={ this.setInputValue }
                  required true
                />
              </FormGroup>
            </Row>
            <Row>
              <Col md={6}>
                <FormGroup>
                  <Checkbox
                    className='RememberMe'
                    type='checkbox'
                    id='remember_me'
                    name='remember_me'
                    onChange={ this.toggleRememberMe }>Remember Me
                  </Checkbox>
                </FormGroup>
              </Col>
              <Col md={6}>
                <a href='/forgot_password'>Did you forget password?</a>
              </Col>
            </Row>
            <Row>
              <Button type='submit' className='btn btn-success btn-block'>Login</Button>
            </Row>
          </form>
        </Col>
      </div>
    )
  }

  defaultState() {
    return {
      email: null,
      password: null,
      remember_me: false,
      errors: null,
      loading: false,
      formSubmitted: false
    }
  }

  constructor(props) {
    super(props)

    this.state = this.defaultState()
    this.setInputValue = this.setInputValue.bind(this)
    this.toggleRememberMe = this.toggleRememberMe.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  setInputValue(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  toggleRememberMe() {
    this.setState({
      remember_me: !this.state.remember_me
    });
  }

  handleSubmit(event) {
    event.preventDefault()
    this.setState({
      formSubmitted: true,
      errors: null,
      loading: true
    })

    let email = this.state.email
    let password = this.state.password
    let remember_me = this.state.remember_me

    Api.authenticateUser(email, password, remember_me).then(response => {
      if (typeof response.error === 'undefined') {
        this.props.propagateSession(response, this.props.history)
      }
      else {
        this.setState({
          errors: response.error,
          loading: false
        })
      }
    })
  } 
}

export default withRouter(LogInComponent);
