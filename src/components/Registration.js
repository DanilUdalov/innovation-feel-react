import React, { Component } from 'react'
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel, Button, Alert, Checkbox } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'
import { RingLoader } from 'react-spinners'

const Api = require('../lib/Api.js')

class RegistrationComponent extends Component {
  render() {
    return (
      <div className='Registration'>
        <Col md={6} mdOffset={2} xs={12}>
          <Row>
            {this.state.errors && this.state.formSubmitted &&
              <Alert bsStyle='danger'>
                <p>{ this.state.errors }</p>
              </Alert>
            }
          </Row>
          <Row>
            {this.state.loading == true &&
              <center>
                <div className='sweet-loading'>
                  <RingLoader
                    color={'#ff005c'}
                    loading={ this.state.loading }
                  />
                </div>
              </center>
            }
          </Row>
          <form onSubmit={ this.handleSubmit }>
            <Row>
              <FormGroup className='col-md-6'>
                <ControlLabel>First name</ControlLabel>
                <FormControl
                  id='first_name'
                  label='First name'
                  name='first_name'
                  placeholder='Enter first name (optional)'
                  onChange={ this.setInputValue }
                />
              </FormGroup>
              <FormGroup className='col-md-6'>
                <ControlLabel>Surname</ControlLabel>
                <FormControl
                  id='surname'
                  label='Surname'
                  name='surname'
                  placeholder="Enter surname (optional)"
                  onChange={ this.setInputValue }
                />
              </FormGroup>
            </Row>
            <Row>
              <FormGroup className='col-md-6'>
                <ControlLabel>Username</ControlLabel>
                <FormControl
                  id='username'
                  label='Username'
                  name='username'
                  placeholder='Enter username (optional)'
                  onChange={ this.setInputValue }
                />
              </FormGroup>
              <FormGroup className='col-md-6'>
                <ControlLabel>Birthday</ControlLabel>
                <FormControl
                  id='birthday'
                  label='Birthday'
                  name='birthday'
                  type='date'
                  placeholder='Enter birthday (optional)'
                  onChange={ this.setInputValue }
                />
              </FormGroup>
            </Row>
            <Row>
              <FormGroup className='col-md-12'>
                <ControlLabel>Email</ControlLabel>
                <FormControl
                  id='email'
                  type='email'
                  label='Email'
                  placeholder='Enter email'
                  name='email'
                  onChange={ this.setInputValue }
                  required true
                />
              </FormGroup>
            </Row>
            <Row>
              <FormGroup className='col-md-6'>
                <ControlLabel>Password</ControlLabel>
                <FormControl
                  id='password'
                  type='password'
                  label='Password'
                  placeholder='Enter password'
                  name='password'
                  onChange={ this.setInputValue }
                  required true
                />
              </FormGroup>
              <FormGroup className='col-md-6'>
                <ControlLabel>Password confirmation</ControlLabel>
                <FormControl
                  id='password_confirmation'
                  type='password'
                  label='password_confirmation'
                  placeholder='Confirm password'
                  name='password_confirmation'
                  onChange={ this.setInputValue }
                  required true
                />
              </FormGroup>
            </Row>
            <Button type='submit' className='btn btn-primary'>Registration now!</Button>
          </form>
        </Col>
      </div>
    )
  }

  defaultState() {
    return {
      email: null,
      password: null,
      password_confirmation: null,
      first_name: null,
      surname: null,
      username: null,
      birthday: null,
      loading: false,
      errors: null,
      formSubmitted: false
    }
  }

  constructor(props) {
    super(props)

    this.state = this.defaultState()
    this.setInputValue = this.setInputValue.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  setInputValue(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  handleSubmit(event) {
    event.preventDefault()
    this.setState({
      formSubmitted: true,
      errors: null,
      loading: true
    })

    let email = this.state.email
    let password = this.state.password
    let password_confirmation = this.state.password_confirmation
    let first_name = this.state.first_name
    let surname = this.state.surname
    let username = this.state.username
    let birthday = this.state.birthday
    let errors = [];

    Api.registrationUser(email, password, password_confirmation, first_name, surname, username, birthday).then(response => {
      if (typeof response.error == 'undefined') {
        this.props.propagateSession(response, this.props.history)
      }
      else if (response.error.hasOwnProperty('password_confirmation')) {
        for (var key in response.error) {
          errors.push(key + ': ' + response.error[key] + '\n')
        }
        this.setState({
          errors: errors,
          loading: false
        })
      }
      else {
        this.setState({
          errors: errors,
          loading: false
        })
      }
    })
  } 
}

export default withRouter(RegistrationComponent)
