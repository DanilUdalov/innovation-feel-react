import React, { Component } from 'react'
import { withRouter } from "react-router-dom"
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel, Button, Alert, Checkbox } from 'react-bootstrap'
import { RingLoader } from 'react-spinners'

const Api = require('../lib/Api.js')

class ResetPasswordContainer extends Component {
  render() {
    return (
      <div className='SetPassword'>
        <Col md={4} mdOffset={3} xs={12}>
          <Row>
            {this.state.errors && this.state.formSubmitted &&
              <Alert bsStyle='danger'>
                <p>{ this.state.errors }</p>
              </Alert>
            }
          </Row>
          <Row>
            {this.state.loading == true &&
              <center>
                <div className='sweet-loading'>
                  <RingLoader
                    color={'#ff005c'}
                    loading={ this.state.loading }
                  />
                </div>
              </center>
            }
          </Row>
          <h3>Set new password</h3>
          <form onSubmit={ this.handleSubmit }>
            <Row>
              <FormGroup>
                <ControlLabel>Password</ControlLabel>
                <FormControl
                  id='password'
                  type='password'
                  label='Password'
                  name='password'
                  placeholder='Enter password'
                  onChange={ this.setInputValue }
                  required true
                />
              </FormGroup>
            </Row>
            <Row>
              <FormGroup>
                <ControlLabel>Password confirmation</ControlLabel>
                <FormControl
                  id='password_confirmation'
                  type='password'
                  label='Password'
                  name='password_confirmation'
                  placeholder='Confirm password'
                  onChange={ this.setInputValue }
                  required true
                />
              </FormGroup>
            </Row>
            <Row>
              <Button type='submit' className='btn btn-danger'>OK</Button>
            </Row>
          </form>
        </Col>
      </div>
    )
  }

  redirectToLogInPage() {
    this.props.history.push('/log_in');
  }

  defaultState() {
    return {
      password: null,
      password_confirmation: null,
      loading: false,
      errors: null,
      formSubmitted: false
    }
  }

  setInputValue(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  constructor(props) {
    super(props)

    this.state = this.defaultState()
    this.handleSubmit = this.handleSubmit.bind(this)
    this.setInputValue = this.setInputValue.bind(this)
    this.setPasswordToken = props.match.params.password_reset_token
  }

  handleSubmit(event) {
    event.preventDefault()
    this.setState({
      formSubmitted: true,
      errors: null,
      loading: true
    })

    let password = this.state.password
    let password_confirmation = this.state.password_confirmation
    let password_token = this.setPasswordToken
    let errors = [];

    Api.setPassword(password, password_confirmation, password_token).then(message => {
      if (typeof message.error == 'undefined') {
        alert(message);
        this.redirectToLogInPage();
      }
      else if (message.error.hasOwnProperty('password_confirmation')) {
        for (var key in message.error) {
          errors.push(key + ': ' + message.error[key] + '\n')
        }
        this.setState({
          errors: errors,
          loading: false
        })
      }
      else {
        this.setState({
          errors: message.error,
          loading: false
        })
      }
    })
  } 
}

export default withRouter(ResetPasswordContainer)
