import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

class SignOutComponent extends Component {
  render() {
    return null
  }

  constructor(props) {
    super(props)
    this.props.propagateSignOut(this.props.history)
  }
}

export default withRouter(SignOutComponent)
