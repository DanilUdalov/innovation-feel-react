import React, { Component } from 'react'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Redirect } from 'react-router'
import { Switch, withRouter } from 'react-router-dom'
import { Col } from 'react-bootstrap'

import Welcome from './Welcome.js'
import AppHeader from './AppHeader.js'
import LogIn from './LogIn.js'
import Registration from './Registration.js'
import ForgotPassword from './ForgotPassword.js'
import ResetPassword from './ResetPassword.js'
import SignOut from './SignOut.js'

import AdminPanel from './admin/Admin.js'

import Articles from './articles/Index.js'
import ShowArticle from './articles/Show.js'
import NewArticle from './articles/New.js'
import UpdateArticle from './articles/Update.js'

const Api = require('../lib/Api.js')

class TokenAuthComponent extends Component {
  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  }

  render() {
    return (
      <Router>
        <div>
          { this.state.header_visible == true &&
            <Col md={12}>
              <AppHeader appState={this.state} />
            </Col>
          }
          <Switch>
            <Route exact path='/admin' render={(routeProps) => (
              this.state.auth_token && this.state.role == 'admin' ? (
                <AdminPanel {...routeProps} />
              ) : (
                <Redirect from="/admin" to="/"/>
              )
            )}/>
            <div className='container'>
              <Route exact path='/' component={Welcome} />
              <Route exact path='/articles' component={Articles} />

              <Route exact path='/articles/new' render={(routeProps) => (
                this.state.auth_token && (this.state.role == 'writter' || this.state.role == 'admin') ? (
                  <NewArticle {...routeProps} />
                ) : (
                  <Redirect from="/articles/new" to="/"/>
                )
              )}/>

              <Route exact path='/articles/page/:number' component={Articles} />

              <Route exact path='/articles/show/:id'
                render={(routeProps) => (
                  <ShowArticle {...routeProps} appState={this.state} />
                )}
              />

              <Route exact path='/articles/edit/:id' render={(routeProps) => (
                this.state.auth_token ? (
                  <UpdateArticle {...routeProps} articleEditingError={this.articleEditingError}  />
                ) : (
                  <Redirect from='/articles/edit/:id' to="/"/>
                )
              )}/>

              <Route exact path='/log_in' render={(routeProps) => (
                !this.state.auth_token ? (
                  <LogIn {...routeProps} propagateSession={this.propagateSession} />
                ) : (
                  <Redirect from="/log_in" to="/"/>
                )
              )}/>

              <Route exact path='/registration' render={(routeProps) => (
                !this.state.auth_token ? (
                  <Registration {...routeProps} propagateSession={this.propagateSession} />
                ) : (
                  <Redirect from="/registration" to="/"/>
                )
              )}/>

              <Route exact path='/forgot_password' render={(routeProps) => (
                !this.state.auth_token ? (
                  <ForgotPassword {...routeProps} propagateForgotPassword={this.propagateForgotPassword} />
                ) : (
                  <Redirect from="/forgot_password" to="/"/>
                )
              )}/>

              <Route exact path='/passwords/:password_reset_token/set_new_password' render={(routeProps) => (
                !this.state.auth_token ? (
                  <ResetPassword {...routeProps} propagateResetPassword={this.propagateSetPassword}/>
                ) : (
                  <Redirect from="/passwords/:password_reset_token/set_new_password" to="/"/>
                )
              )}/>

              <Route exact path='/sign_out' render={(routeProps) => (
                this.state.auth_token ? (
                  <SignOut {...routeProps} propagateSignOut={this.propagateSignOut} />
                ) : (
                  <Redirect from="/sign_out" to="/"/>
                )
              )}/>
            </div>
          </Switch>
        </div>
      </Router>
    )
  }

  componentDidMount() {
    this.getUser()
    this.excludeHeader()
  }

  defaultState() {
    return {
      cookieName: 'rails_react_token_auth_token',
      email: undefined,
      first_name: undefined,
      surname: undefined,
      username: undefined,
      birthday: undefined,
      user_id: undefined,
      auth_token: document.cookie.split('rails_react_token_auth_token=')[1] || undefined,
      role: (JSON.parse(localStorage.getItem('user')) || {}).role,
      notification: undefined,
      header_visible: true,
      alert_style: undefined
    }
  }

  constructor(props) {
    super(props)

    this.state = this.defaultState()
    this.propagateSession = this.propagateSession.bind(this)
    this.propagateSignOut = this.propagateSignOut.bind(this)
    this.excludeHeader    = this.excludeHeader.bind(this)
    this.articleEditingError  = this.articleEditingError.bind(this)
  }

  excludeHeader() {
    if (window.location.pathname == '/admin') {
      this.setState({ header_visible: false })
    }
  }

  articleEditingError(history = undefined) {
    this.setState({
      notification: 'You don`t have access permission for editing this article!',
      alert_style: 'danger'
    })
    if (history) history.push('/')
    this.clearNotification()
  }

  propagateSession(response, history = undefined) {
    const { cookies } = this.props
    cookies.set(this.state.cookieName, response.auth_token, { path: '/' })
    localStorage.setItem('user', JSON.stringify(response));
    if (response !== undefined) {
      this.setState({
        email: response.email,
        user_id: response.id,
        first_name: response.first_name,
        surname: response.surname,
        birthday: response.birthday,
        username: response.username,
        auth_token: response.auth_token,
        role: response.role,
        notification: 'Welcome ' + response.email,
        alert_style: 'success'
      })
      if (history) history.push('/')
      this.clearNotification()
    }
  }

  propagateSignOut(history = undefined) {
    const { cookies } = this.props
    Api.logOutUser(cookies.get(this.state.cookieName))
    cookies.remove(this.state.cookieName, { path: '/' })
    localStorage.clear()
    this.setState({
      email: undefined,
      first_name: undefined,
      surname: undefined,
      username: undefined,
      user_id: undefined,
      auth_token: undefined,
      role: undefined,
      notification: 'Good bye!',
      alert_style: 'success'
    })
    if (history) history.push('/')
    this.clearNotification()
  }

  getUser(history = undefined) {
    const { cookies } = this.props
    let auth_token = cookies.get(this.state.cookieName)
    if (!auth_token) return null
    var current_user = JSON.parse(localStorage.getItem('user'))
    this.setState({
      auth_token: auth_token,
      email: current_user.email,
      first_name: current_user.first_name,
      surname: current_user.surname,
      username: current_user.username,
      birthday: current_user.birthday,
      user_id: current_user.id,
      role: current_user.role
    })
  }

  clearNotification() {
    setTimeout(function(){
      this.setState({ notification: undefined });
    }.bind(this), 5000);
  }
}

export default withCookies(TokenAuthComponent)
