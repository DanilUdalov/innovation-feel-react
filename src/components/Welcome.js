import React, { Component } from 'react'
import { Col, Jumbotron } from 'react-bootstrap'
import WoodyPicture from './images/5890d8ddb2cb5b18709ca621.png'

class WelcomeComponent extends Component {
  render() {
    return (
      <div>
        <Col md={12}>
          <Jumbotron>
            <center>
              <h2>Welcome to articles application</h2>
              <p>
                <img src={WoodyPicture} className="woodyPicture" alt="logo" />
              </p>
            </center>
          </Jumbotron>
        </Col>
      </div>
    )
  }
}

export default WelcomeComponent
