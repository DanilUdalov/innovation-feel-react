import React, { Component } from "react"
import { fetchUtils, Admin, Resource } from "react-admin"
import { CookiesProvider } from "react-cookie"

import ArticleIcon from "@material-ui/icons/Book"
import UserIcon from "@material-ui/icons/People"
import CommentIcon from "@material-ui/icons/SpeakerNotes"
import CategoryIcon from "@material-ui/icons/List"

import Dashboard from "./Dashboard"
import { ArticleList, ArticleShow, ArticleCreate, ArticleEdit } from "./Articles.js"
import { CategoryList, CategoryEdit, CategoryCreate, CategoryShow } from "./Categories.js"
import { CommentList, CommentEdit, CommentShow, CommentCreate } from "./Comments.js"
import { UserList, UserShow, UserEdit } from "./Users.js"

import RestProvider from "../../lib/RestProvider.js"

var hostname = window.location.origin;
var backendHost;

if (hostname == "http://localhost:3000") {
  backendHost = "http://localhost:3001";
} else if (hostname == "https://if-blog-test-staging.herokuapp.com") {
  backendHost = "https://if-blog-article-server-staging.herokuapp.com";
} else if (hostname == "https://if-blog-test.herokuapp.com") {
  backendHost = "https://if-blog-article-server.herokuapp.com";
}

var axios = require("axios");
const Api = require("../../lib/Api.js");

const httpClient = (url, options = {}) => {
  if (!options.headers) {
    options.headers = new Headers({ Accept: "application/json" });
  }

  options.headers.set("Accept-Version", "v1");
  options.headers.set(
    "Auth-Token",
    document.cookie.split("rails_react_token_auth_token=")[1]
  );
  return fetchUtils.fetchJson(url, options);
};

class AppAdmin extends Component {
  render() {
    return (
      <Admin
        dashboard={Dashboard}
        title="IF Blog - Admin dashboard"
        dataProvider={RestProvider(backendHost + "/api", httpClient)}
      >
        <Resource
          name="articles"
          list={ArticleList}
          show={ArticleShow}
          create={ArticleCreate}
          edit={ArticleEdit}
          icon={ArticleIcon}
        />
        <Resource
          name="categories"
          list={CategoryList}
          create={CategoryCreate}
          edit={CategoryEdit}
          show={CategoryShow}
          icon={CategoryIcon}
        />
        <Resource
          name="comments"
          list={CommentList}
          show={CommentShow}
          create={CommentCreate}
          edit={CommentEdit}
          icon={CommentIcon}
        />
        <Resource
          name="users"
          list={UserList}
          show={UserShow}
          edit={UserEdit}
          icon={UserIcon}
        />
      </Admin>
    );
  }
}

export default AppAdmin;
