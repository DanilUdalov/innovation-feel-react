import React from "react"
import {
  List,
  Datagrid,
  downloadCSV,
  Show,
  Create,
  Edit,
  SimpleForm,
  ReferenceField,
  ReferenceInput,
  ShowButton,
  EditButton,
  DeleteButton,
  SimpleShowLayout,
  SelectInput,
  LongTextInput,
  TextField,
  DateField,
  TextInput,
  RichTextField,
  ChipField
} from "react-admin"
import ChangeStatusButton from "./custom_actions/ChangeStatusButton.js"
import { unparse as convertToCSV } from "papaparse/papaparse.min"

const Api = require("../../lib/Api.js")

const exporter = () => {
  let auth_token = document.cookie.split("rails_react_token_auth_token=")[1];

  Api.getAllArticles(auth_token).then(articles => {
    const fields = [
      "id",
      "title",
      "body",
      "comments_count",
      "author_email",
      "author_role",
      "category_id",
      "category_name",
      "posted_date",
      "status"
    ];
    const data = articles.map(record => ({
      id: record.id,
      title: record.title,
      body: record.body,
      comments_count: record.comments_count,
      category_id: record.category.id,
      category_name: record.category.name,
      posted_date: record.posted_date,
      status: record.status,
      author_email: record.user.email,
      author_role: record.user.role
    }));
    const csv = convertToCSV({
      data,
      fields: fields
    });
    downloadCSV(csv, `articles_${Date.now()}`);
  });
};

const validateArticle = values => {
  const errors = {};
  if (!values.title) {
    errors.title = ["The title is required"];
  }
  if (!values.body) {
    errors.body = ["The body is required"];
  }
  if (!values.category_id) {
    errors.category_id = ["The category_id is required"];
  }
  return errors;
};

export const ArticleList = props => (
  <List {...props} sort={{ field: "id", order: "ASC" }} exporter={exporter}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="title" />
      <TextField source="user.email" label="Author" />
      <TextField source="comments_count" label="Count comments" />
      <ReferenceField
        label="Category"
        source="category.id"
        reference="categories"
        linkType="show"
      >
        <ChipField source="name" />
      </ReferenceField>
      <TextField source="posted_date" sortable={false} />
      <TextField source="status" />
      <ChangeStatusButton />
      <ShowButton />
      <EditButton />
      <DeleteButton />
    </Datagrid>
  </List>
);

export const ArticleCreate = props => (
  <Create title="Create an Article" {...props}>
    <SimpleForm redirect={`/articles/`} validate={validateArticle}>
      <TextInput source="title" />
      <LongTextInput source="body" />
      <ReferenceInput
        label="Category"
        source="category_id"
        reference="categories"
        perPage={100}
      >
        <SelectInput optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Create>
);

export const ArticleEdit = props => (
  <Edit {...props}>
    <SimpleForm redirect={`/articles/`} validate={validateArticle}>
      <TextInput source="title" />
      <LongTextInput source="body" />
      <ReferenceInput
        label="Category"
        source="category_id"
        reference="categories"
        perPage={100}
      >
        <SelectInput optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);

export const ArticleShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="title" />
      <RichTextField source="body" />
      <RichTextField source="category.name" label="Category" />
      <TextField source="comments_count" />
      <DateField label="Posted date" source="posted_date" />
      <TextField source="status" />
    </SimpleShowLayout>
  </Show>
);
