import React from "react"
import {
  List,
  Datagrid,
  downloadCSV,
  Edit,
  Create,
  Show,
  SimpleForm,
  SimpleShowLayout,
  DateField,
  TextField,
  ShowButton,
  EditButton,
  DeleteButton,
  DisabledInput,
  TextInput,
  LongTextInput,
  DateInput
} from "react-admin"
import { unparse as convertToCSV } from "papaparse/papaparse.min"

const Api = require("../../lib/Api.js")

const CategoryTitle = ({ record }) => {
  return <span>Category {record ? `"${record.name}"` : ""}</span>;
};

const exporter = () => {
  let auth_token = document.cookie.split("rails_react_token_auth_token=")[1];

  Api.getAllCategories(auth_token).then(categories => {
    const data = categories.map(record => ({
      id: record.id,
      name: record.name,
      articles_count: record.articles_count,
      created_at: record.created_at,
      updated_at: record.updated_at
    }));
    const csv = convertToCSV({
      data,
      fields: ["id", "name", "articles_count", "created_at", "updated_at"]
    });
    downloadCSV(csv, `categories_${Date.now()}`);
  });
};

const validateCategory = values => {
  const errors = {};
  if (!values.name) {
    errors.name = ["The name is required"];
  }
  return errors;
};

export const CategoryList = props => (
  <List
    {...props}
    perPage={10}
    sort={{ field: "articles_count", order: "DESC" }}
    exporter={exporter}
  >
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <TextField source="articles_count" />
      <TextField source="created_at" />
      <TextField source="updated_at" />
      <ShowButton />
      <EditButton />
      <DeleteButton />
    </Datagrid>
  </List>
);

export const CategoryCreate = props => (
  <Create title="Create a Category" {...props}>
    <SimpleForm redirect={`/categories`} validate={validateCategory}>
      <TextInput source="name" />
    </SimpleForm>
  </Create>
);

export const CategoryEdit = props => (
  <Edit title={<CategoryTitle />} {...props}>
    <SimpleForm redirect={`/categories`} validate={validateCategory}>
      <TextInput source="name" />
    </SimpleForm>
  </Edit>
);

export const CategoryShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="name" />
      <TextField source="articles_count" />
      <TextField source="created_at" />
      <TextField source="updated_at" />
    </SimpleShowLayout>
  </Show>
);
