import React from "react"
import {
  List,
  Edit,
  Show,
  Create,
  downloadCSV,
  Datagrid,
  LongTextInput,
  TextField,
  SelectInput,
  TextInput,
  ReferenceField,
  ChipField,
  ReferenceInput,
  ShowButton,
  EditButton,
  DeleteButton,
  SimpleShowLayout,
  SimpleForm
} from "react-admin"

import { unparse as convertToCSV } from "papaparse/papaparse.min"

const Api = require("../../lib/Api.js")

const exporter = () => {
  let auth_token = document.cookie.split("rails_react_token_auth_token=")[1];

  Api.getAllComments(auth_token).then(articles => {
    const fields = [
      "id",
      "body",
      "article_id",
      "article_title",
      "user_id",
      "user_email",
      "user_role",
      "created_at",
      "updated_at"
    ];
    const data = articles.map(record => ({
      id: record.id,
      body: record.body,
      article_id: record.article.id,
      article_title: record.article.title,
      user_id: record.user.id,
      user_email: record.user.email,
      user_role: record.user.role,
      created_at: record.created_at,
      updated_at: record.updated_at
    }));
    const csv = convertToCSV({
      data,
      fields: fields
    });
    downloadCSV(csv, `comments_${Date.now()}`);
  });
};

const validateComment = values => {
  const errors = {};
  if (!values.body) {
    errors.body = ["The body is required"];
  }
  if (!values.article_id) {
    errors.article_id = ["The article_id is required"];
  }
  return errors;
};

export const CommentList = props => (
  <List
    {...props}
    perPage={10}
    sort={{ field: "id", order: "ASC" }}
    exporter={exporter}
  >
    <Datagrid>
      <TextField source="id" />
      <TextField source="body" />
      <ReferenceField label="Article" source="article.id" reference="articles" linkType="show">
        <TextField source="id" />
      </ReferenceField>
      <ReferenceField label="User" source="user.id" reference="users" linkType="show">
        <TextField source="email" />
      </ReferenceField>
      <TextField source="created_at" />
      <TextField source="updated_at" />
      <ShowButton />
      <EditButton />
      <DeleteButton />
    </Datagrid>
  </List>
);

export const CommentCreate = props => (
  <Create title="Create a Comment" {...props}>
    <SimpleForm redirect={`/comments`} validate={validateComment}>
      <LongTextInput source="body" />
      <ReferenceInput
        label="Article"
        source="article_id"
        reference="articles"
        sort={{ field: "id", order: "ASC" }}
        perPage={100}
      >
        <SelectInput optionText="id" />
      </ReferenceInput>
    </SimpleForm>
  </Create>
);

export const CommentEdit = props => (
  <Edit {...props}>
    <SimpleForm redirect={`/comments`} validate={validateComment}>
      <LongTextInput source="body" />
      <ReferenceInput
        label="Article"
        source="article_id"
        reference="articles"
        sort={{ field: "id", order: "ASC" }}
        perPage={100}
      >
        <SelectInput optionText="id" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);

export const CommentShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="body" />
      <ReferenceField label="Article" source="article.id" reference="articles" linkType="show">
        <TextField source="id" />
      </ReferenceField>
      <TextField source="user.id" />
      <ReferenceField label="User" source="user.id" reference="users" linkType="show">
        <TextField source="id" />
      </ReferenceField>
    </SimpleShowLayout>
  </Show>
);
