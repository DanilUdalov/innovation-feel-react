import React from "react"
import Card from "@material-ui/core/Card"
import CardContent from "@material-ui/core/CardContent"
import { ViewTitle } from "react-admin"

export default () => (
  <Card>
    <center>
      <CardContent>
        <ViewTitle title="Welcome to the administration" />
        <p>If you are looking at this page, then you are real admin!</p>
        <a href="/">Back to main page</a>
      </CardContent>
    </center>
  </Card>
);
