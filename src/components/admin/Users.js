import React from "react"
import {
  List,
  Show,
  Edit,
  SimpleForm,
  downloadCSV,
  SimpleShowLayout,
  Datagrid,
  TextField,
  EmailInput,
  TextInput,
  DateInput,
  ShowButton,
  DeleteButton,
  EditButton
} from "react-admin"
import ChangeUserRoleButton from "./custom_actions/ChangeUserRoleButton.js"
import { unparse as convertToCSV } from "papaparse/papaparse.min"

const Api = require("../../lib/Api.js")

const exporter = () => {
  let auth_token = document.cookie.split("rails_react_token_auth_token=")[1];

  Api.getAllUsers(auth_token).then(users => {
    const fields = [
      "id",
      "email",
      "first_name",
      "surname",
      "username",
      "birthday",
      "role",
      "created_at",
      "updated_at"
    ];
    const data = users.map(record => ({
      id: record.id,
      email: record.email,
      first_name: record.first_name,
      surname: record.surname,
      username: record.username,
      birthday: record.birthday,
      role: record.role,
      created_at: record.created_at,
      updated_at: record.updated_at
    }));
    const csv = convertToCSV({
      data,
      fields: fields
    });
    downloadCSV(csv, `users_${Date.now()}`);
  });
};

export const UserList = props => (
  <List
    {...props}
    perPage={10}
    sort={{ field: "id", order: "ASC" }}
    exporter={exporter}
  >
    <Datagrid>
      <TextField source="id" />
      <TextField source="first_name" />
      <TextField source="surname" />
      <TextField source="username" />
      <TextField source="birthday" />
      <TextField source="email" />
      <TextField source="created_at" />
      <TextField source="updated_at" />
      <TextField source="role" />
      <ChangeUserRoleButton />
      <ShowButton />
      <EditButton />
      <DeleteButton />
    </Datagrid>
  </List>
);

const validateUser = values => {
  const errors = {};
  if (!values.email) {
    errors.email = ["The email is required"];
  }
  return errors;
};

export const UserShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="first_name" />
      <TextField source="surname" />
      <TextField source="username" />
      <TextField source="birthday" />
      <TextField source="email" />
      <TextField source="created_at" />
      <TextField source="updated_at" />
      <TextField source="role" />
    </SimpleShowLayout>
  </Show>
);

export const UserEdit = props => (
  <Edit {...props}>
    <SimpleForm redirect={`/users`} validate={validateUser}>
      <TextInput source="first_name" />
      <TextInput source="surname" />
      <TextInput source="username" />
      <DateInput source="birthday" />
      <TextInput source="email" />
      <TextInput source="password" />
      <TextInput source="password_confirmation" />
    </SimpleForm>
  </Edit>
);
