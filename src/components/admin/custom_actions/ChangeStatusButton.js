import React, { Component } from 'react'
import { MenuItem, DropdownButton } from 'react-bootstrap'

import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { showNotification as showNotificationAction } from 'react-admin'
import { push as pushAction } from 'react-router-redux'

const Api = require('../../../lib/Api.js')

class ChangeStatusButton extends Component {
  handleStatus = (event) => {
    const { push, record, showNotification } = this.props;

    let id = record.id
    let auth_token = document.cookie.split('rails_react_token_auth_token=')[1]
    let status = event.target.text

    Api.updateArticleStatus(auth_token, status, id).then(() => {
        showNotification('Article is ' + status);
        push('/articles');
      })
      .catch((e) => {
        showNotification('Error: article is not ' + status, 'warning')
      });
  }

  render() {
    return (
      <DropdownButton bsStyle='warning' className='btn btn-sm' title='Change status'>
        <MenuItem value="pending" onClick={this.handleStatus}>pending</MenuItem>
        <MenuItem value="blocked" onClick={this.handleStatus}>blocked</MenuItem>
        <MenuItem value="posted" onClick={this.handleStatus}>posted</MenuItem>
      </DropdownButton>
    )
  }

  constructor(props) {
    super(props)

    this.handleStatus = this.handleStatus.bind(this)
  }
}

ChangeStatusButton.propTypes = {
  push: PropTypes.func,
  record: PropTypes.object,
  showNotification: PropTypes.func,
};

export default connect(null, {
  showNotification: showNotificationAction,
  push: pushAction,
})(ChangeStatusButton);
