import React, { Component } from 'react'
import { MenuItem, DropdownButton } from 'react-bootstrap'

import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { showNotification as showNotificationAction } from 'react-admin'
import { push as pushAction } from 'react-router-redux'

const Api = require('../../../lib/Api.js')

class ChangeUserRoleButton extends Component {
  handleRole = (event) => {
    const { push, record, showNotification } = this.props;

    let id = record.id
    let auth_token = document.cookie.split('rails_react_token_auth_token=')[1]
    let role = event.target.text

    Api.updateUserRole(auth_token, role, id).then(() => {
        showNotification('User is ' + role);
        push('/users');
      })
      .catch((e) => {
        showNotification('Error: user role changing was failed', 'warning')
      });
  }

  render() {
    return (
      <DropdownButton bsStyle='primary' className='btn btn-sm' title='Change role'>
        <MenuItem value="writter" onClick={this.handleRole}>writter</MenuItem>
        <MenuItem value="moderator" onClick={this.handleRole}>moderator</MenuItem>
        <MenuItem value="admin" onClick={this.handleRole}>admin</MenuItem>
      </DropdownButton>
    )
  }

  constructor(props) {
    super(props)

    this.handleRole = this.handleRole.bind(this)
  }
}

ChangeUserRoleButton.propTypes = {
  push: PropTypes.func,
  record: PropTypes.object,
  showNotification: PropTypes.func,
};

export default connect(null, {
  showNotification: showNotificationAction,
  push: pushAction,
})(ChangeUserRoleButton);
