import React, { Component } from 'react'
import { Row, Col, FormGroup, FormControl, Label, Button, Alert, ControlLabel } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'
import Dropzone from 'react-dropzone'

import CKEditor from 'react-ckeditor-component'

const Api = require('../../lib/Api.js')

class ArticleFormComponent extends Component {
  render() {
    return(
      <Row>
        <Col md={12} xs={12}>
          <Row>
            <Col md={12} xs={12}>
              {this.state.errors && this.state.formSubmitted &&
                <Alert className='alert-with-errors' bsStyle='danger'>
                  <center>
                    <h3 className='alert-heading'>ERROR</h3>
                    <p>{this.state.errors}</p>
                  </center>
                </Alert>
              }
            </Col>
          </Row>
          <form onSubmit={this.handleSubmit}>
            <Row>
              <Col md={12} xs={12}>
                <FormGroup>
                  <ControlLabel>Title</ControlLabel>
                  <FormControl
                    id='title'
                    label='Title'
                    name='title'
                    placeholder='Enter title'
                    value={this.state.title}
                    onChange={this.handleChange}
                    required true
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col md={12} xs={12}>
                <FormGroup>
                  <ControlLabel>Category</ControlLabel>
                  <FormControl value={this.state.category_id} componentClass='select' id='category_id' name='category_id' onChange={ this.handleChange }>
                    {this.state.categories.map(category =>
                      <option value={category.id}>{category.name}</option>
                    )}
                  </FormControl>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col md={12} xs={12}>
                <FormGroup>
                  <ControlLabel>Text</ControlLabel>
                  <CKEditor
                    activeClass='p10'
                    content={this.state.body}
                    events={{
                      'change': this.handleChange
                    }}
                  />
                </FormGroup>
              </Col>
            </Row>
            <br />
            <Row>
              <Col md={4} xs={6}>
                <p>{this.state.upload_error != null && this.state.upload_error}</p>
                <Dropzone onDrop={this.readFile} accept="image/png,image/jpeg,image/jpg" maxSize={5000000}>
                  <Button>Upload images</Button>
                  <br/>
                  {'Count images: ' + this.state.images_count }
                </Dropzone>
              </Col>
              <Col md={8} xs={6}>
                { window.location.pathname.includes('/articles/edit') &&
                  Object.keys(this.state.exists_images).map(key => {
                    return(
                      <Row id={'nested_image_' + this.state.exists_images[key].id}>
                        <Col md={3} xs={3}>
                          <p><img src={this.state.exists_images[key].picture.thumbnail['url']} /></p>
                        </Col>
                        <Col md={9} xs={9}>
                          <Button
                            bsStyle='danger'
                            id={'image_destroy_' + this.state.exists_images[key].id}
                            className='btn-sm'
                            onClick={this.deleteImage}>Destroy</Button>
                        </Col>
                      </Row>
                    )
                  })
                }
                {this.state.images != null && this.state.images.map(image =>
                  <Row key={image.name}>
                    <img className='upload_image' src={image.preview} /> - {image.size} bytes
                  </Row>
                )}
              </Col>
            </Row>
            <br/>
            <Row>
              <Col md={12} xs={12}>
                {!this.state.loading ? (
                  <Button type='submit' className='btn btn-success pull-left'>OK</Button>
                ) : (
                  <Button className="btn btn-success pull-left" disabled>
                    <span className="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...
                  </Button>
                )}
              </Col>
            </Row>
          </form>
        </Col>
      </Row>
    )
  }

  defaultState() {
    return {
      id: this.props.match.params.id || null,
      title: this.props.appState.title || null,
      body: this.props.appState.body || null,
      images: [],
      exists_images: this.props.appState.images || [],
      exists_images_count: (this.props.appState.images || {}).length || 0, // It is count of exists images
      images_count: (this.props.appState.images || {}).length || 0, // It is count of all images
      upload_error: null,
      preview: '',
      category_id: this.props.appState.category_id || null,
      categories: this.props.appState.categories || [],
      visible: true,
      loading: false,
      errors: this.props.appState.errors || null,
      notification: null,
      formSubmitted: false,
      auth_token: document.cookie.split('rails_react_token_auth_token=')[1] || null
    }
  }

  constructor(props) {
    super(props)

    this.state = this.defaultState()
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.readFile = this.readFile.bind(this)
    this.handleGotResult = this.handleGotResult.bind(this)
    this.deleteImage = this.deleteImage.bind(this)
    this.createSomeImages = this.createSomeImages.bind(this)
  }

  handleChange(event) {
    if (event.editor) {
      this.setState({body: event.editor.getData()})
    } else {
      this.setState({[event.target.name]: event.target.value})
    }
  }

  readFile(files) {
    var count_images = this.state.exists_images_count + files.length

    if (files && files[0] && count_images <= 5) {
      this.setState({
        images: files,
        upload_error: null,
        images_count: count_images
      })
    } else if (count_images > 5) {
      this.setState({
        images: null,
        upload_error: 'Error! An article must hold less than 5 image',
        images_count: this.state.exists_images.length
      })
    } else {
      this.setState({
        images: null,
        upload_error: 'Error! Your file is not valid!',
        images_count: this.state.exists_images.length
      })
    }
  }

  deleteImage(event) {
    if (window.confirm('Are you really going to delete this image?')) {
      let auth_token = this.state.auth_token
      let id = event.target.id.split('image_destroy_')[1]
      let image_count = this.state.images_count
      let exists_images_count = this.state.exists_images_count

      if (id != null) {
        Api.destroyImage(auth_token, id).then(response => {
          document.getElementById('nested_image_' + id).remove()
          this.setState({
            images_count: image_count - 1,
            exists_images_count: exists_images_count - 1
          })
        })
      }
      else {
        alert('Error! Not correct value!')
      }
    }
  }

  handleGotResult(response) {
    let errors = []

    if (response.id) {
      this.setState({ id: response.id}, function () {
        this.createSomeImages(response)
      })
    }
    else {
      for (var key in response.errors) { errors.push(key + ': ' + response.errors[key] + '\n') }
      this.setState({
        errors: errors,
        loading: false
      })
      document.body.scrollTop = document.documentElement.scrollTop = 0
    }
  }

  createSomeImages(response) {
    let images = this.state.images
    let exists_images = this.state.exists_images.length
    let article_id = response.id

    images.map(image => {
      Api.createImage(this.state.auth_token, image, article_id).then(response => {
        if(response.article.images_count == this.state.images_count) {
          this.props.history.push('/articles/show/' + article_id)
        }
      })
    })
  }

  handleSubmit(event) {
    event.preventDefault()

    this.setState({
      formSubmitted: true,
      loading: true
    })

    let auth_token      = this.state.auth_token
    let path            = this.props.match.path
    let id              = this.state.id
    let title           = this.state.title
    let body            = this.state.body
    let category_id     = this.state.category_id
    var article_request = ''

    if (path == '/articles/new') {
      article_request = Api.createArticle(auth_token, title, body, category_id)
    } else if (path == '/articles/edit/:id') {
      article_request = Api.updateArticle(auth_token, id, title, body, category_id)
    }
    article_request.then(response => {
      if(this.state.images.length != 0) {
        this.handleGotResult(response)
      } else {
        this.props.history.push('/articles/show/' + response.id)
      }
    })
  }
}

export default withRouter(ArticleFormComponent)
