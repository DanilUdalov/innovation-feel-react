import React, { Component } from 'react'
import { Row, Col, Button, Table, DropdownButton, MenuItem, ButtonGroup, Glyphicon, Breadcrumb, Badge } from 'react-bootstrap'
import ReactPaginate from 'react-paginate'
import { LinkContainer } from 'react-router-bootstrap'

import Spinner from '../shared/Spinner.js'

const Api = require('../../lib/Api.js')

class ArticlesContainer extends Component {
  render() {
    return (
      <div>
        <Col md={12} xs={12}>
          <Row>
            <Col md={10} xs={7}>
              <Breadcrumb>
                <li class="active">Articles</li>
              </Breadcrumb>
            </Col>
            <Col md={2} xs={5}>
              {this.state.visible == true &&
                <Badge className='pull-right BadgeTotal'>Total articles: {this.state.article_total}</Badge>
              }
            </Col>
          </Row>
          <Row>
            <Col md={6} xs={6}>
              <DropdownButton bsStyle='danger' className='pull-left' title="Count articles in one page" onSelect={this.setCountArticles}>
                <MenuItem eventKey='10'>10</MenuItem>
                <MenuItem eventKey='15'>15</MenuItem>
                <MenuItem eventKey='20'>20</MenuItem>
              </DropdownButton>
            </Col>
            <Col md={6} xs={6}>
              {this.state.auth_token && (this.state.role == 'writter' || this.state.role == 'admin')  &&
                <LinkContainer exact to="/articles/new">
                  <Button bsStyle='primary' className='pull-right'>New article</Button>
                </LinkContainer>
              }
            </Col>
          </Row>
          <Row>
            <Col md={12} xs={12}>
              <Table responsive>
                <thead>
                  <tr>
                    <th>
                      <a className='sort_link' id="title" onClick={ this.handleSort } title='Sort by title'>
                        Title <Glyphicon className='sort-arrow' id="title" glyph={this.state.sorting.arrow} />
                      </a>
                    </th>
                    <th>Preview</th>
                    <th>
                      <a className='sort_link' id="category" onClick={ this.handleSort } title='Sort by category'>
                        Category <Glyphicon className='sort-arrow' id="category" glyph={this.state.sorting.arrow} />
                      </a>
                    </th>
                    <th>
                      <a className='sort_link' id="user" onClick={ this.handleSort } title='Sort by author'>
                        Who posted <Glyphicon className='sort-arrow' id="user" glyph={this.state.sorting.arrow} />
                      </a>
                    </th>
                    <th>
                      <a className='sort_link' id="created_at" onClick={ this.handleSort } title='Sort by posted date'>
                        Posted date <Glyphicon className='sort-arrow' id="created_at" glyph={this.state.sorting.arrow} />
                      </a>
                    </th>
                    <th>
                      <a className='sort_link' id="status" onClick={ this.handleSort } title='Sort by status'>
                        Status <Glyphicon className='sort-arrow' id="status" glyph={this.state.sorting.arrow} />
                      </a>
                    </th>
                    {this.state.auth_token && <th>Actions</th>}
                  </tr>
                </thead>
                {this.state.visible == true &&
                  <tbody>
                    {Object.keys(this.state.articles).map(key => {
                      return(
                        <tr className="title" key={key}>
                          <td>
                            <a href={'/articles/show/' + this.state.articles[key].id}>
                              {this.state.articles[key].title}
                            </a>
                          </td>
                          <td>
                            {this.state.articles[key].front_image != null &&
                              <img src={this.state.articles[key].front_image.picture.thumbnail.url} />
                            }
                          </td>
                          <td>{this.state.articles[key].category['name']}</td>
                          <td>{this.state.articles[key].user['email']}</td>
                          <td>{this.state.articles[key].posted_date}</td>
                          <td>{this.state.articles[key].status}</td>
                          {this.state.auth_token &&
                            <td>
                              <ButtonGroup>
                                {(this.state.role == 'admin' || this.state.role == 'moderator' || this.state.articles[key].user['id'] == this.state.user_id) &&
                                  <Button bsStyle='warning' href={'/articles/edit/' + this.state.articles[key].id } className='btn-sm'>
                                    <Glyphicon glyph='pencil' />
                                  </Button>
                                }
                                {(this.state.role == 'admin' || this.state.articles[key].user['id'] == this.state.user_id) &&
                                  <Button bsStyle='danger' className='btn-sm' id={'articles_' + this.state.articles[key].id} onClick={this.handleDelete}>
                                    <Glyphicon glyph='trash' id={'articles_' + this.state.articles[key].id} />
                                  </Button>
                                }
                              </ButtonGroup>
                            </td>
                          }
                        </tr>
                      )
                    })}
                  </tbody>
                }
              </Table>
            </Col>
          </Row>
          <Row>
            {this.state.loading == true &&
              <Spinner appState={this.state} />
            }
          </Row>
          {this.state.visible == true &&
            <Row>
              <center>
                <ReactPaginate previousLabel={"<="}
                               nextLabel={"=>"}
                               breakLabel={<a href="">...</a>}
                               breakClassName={"break-me"}
                               pageCount={this.state.page_total}
                               marginPagesDisplayed={2}
                               pageRangeDisplayed={2}
                               forcePage={this.state.page - 1}
                               initialPage={this.props.page}
                               onPageChange={this.getPage}
                               containerClassName={"pagination"}
                               subContainerClassName={"pages pagination"}
                               activeClassName={"active"} />
              </center>
            </Row>
          }
        </Col>
      </div>
    )
  }

  componentDidMount() {
    this.getArticles()
  }

  defaultState() {
    return {
      cookieName: 'rails_react_token_auth_token',
      loading: false,
      articles: '',
      visible: true,
      /* If we don not have page param in the path, then it returns 1 */
      page: this.props.match.params.number || 1,
      rows_per_page: 10,
      page_total: null,
      article_total: null,
      auth_token: document.cookie.split('rails_react_token_auth_token=')[1] || '',
      user_id: null,
      role: null,
      sorting: {
        sort_by: null,
        order: null,
        arrow: 'triangle-top'
      }
    }
  }

  constructor(props) {
    super(props)

    this.state = this.defaultState()
    this.getPage          = this.getPage.bind(this);
    this.setCountArticles = this.setCountArticles.bind(this);
    this.handleDelete     = this.handleDelete.bind(this);
    this.handleSort       = this.handleSort.bind(this);
  }

  setCountArticles(per_page){
    this.setState({ rows_per_page: parseInt(per_page)}, function () {
      this.getArticles(this.state.rows_per_page);
    });
  }

  handleSort(sort_by) {
    this.setState({
      sorting: {
        sort_by: sort_by.target.id,
        order: this.switchParamsSort(this.state.sorting.order),
        arrow: this.switchArrowSort(this.state.sorting.arrow)}},
        function () { this.getArticles(this.state.sorting.sort_by);
    });
  }

  switchParamsSort(param) {
    switch(param) {
      case 'asc':
        return 'desc';
      default:
        return 'asc';
    }
  }

  switchArrowSort(param) {
    switch(param) {
      case 'triangle-top':
        return 'triangle-bottom';
      default:
        return 'triangle-top';
    }
  }

  getPage(page) {
    {/*
      React-paginate have one feature: first paginate button begin with 0 index.
      Therefore, to get the correct page, must add 1 to the page number
    */}
    this.setState({ page: page.selected + 1 }, function () {
      this.getArticles(this.state.page);
    });
  }

  handleDelete(event) {
    if (window.confirm('Are you really going to delete the article?')) {
      let auth_token = this.state.auth_token
      let id = event.target.id.split('articles_')[1]

      Api.destroyArticle(auth_token, id).then(response => {
        this.getArticles();
      })
    }
  }

  getArticles() {
    this.setState({
      loading: true,
      visible: false
    })

    let auth_token = this.state.auth_token
    let page = this.state.page
    let per_page = this.state.rows_per_page
    let sort_by = this.state.sorting.sort_by
    let order = this.state.sorting.order

    var current_user = JSON.parse(localStorage.getItem('user'))
    var user_id = (current_user || {}).id
    var role = (current_user || {}).role

    Api.getArticles(auth_token, page, per_page, sort_by, order).then(response => {
      this.setState({
        articles: response.data,
        loading: false,
        page: response.headers['x-current-page'],
        rows_per_page: response.headers['x-per-page'],
        article_total: response.headers['x-total'],
        page_total: response.headers["x-total-pages"],
        visible: true,
        user_id: user_id,
        role: role
      })
      if (response.headers['x-current-page'] > 1) {
        this.props.history.push('/articles/page/' + response.headers['x-current-page'])
      } else {
        this.props.history.push('/articles')
      }
    })
  }
}

ArticlesContainer.defaultProps = {};

export default ArticlesContainer
