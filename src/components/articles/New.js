import React, { Component } from 'react'
import { Row, Col, Breadcrumb } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'

import ArticleForm from './Form.js'
import Spinner from '../shared/Spinner.js'

const Api = require('../../lib/Api.js')

class NewArticleComponent extends Component {
  render() {
    return (
      <div className='newArticle'>
        <Col md={8} mdOffset={2} xs={12}>
          <Row>
            <Col md={12} xs={12}>
              <Breadcrumb>
                <li><a href='/articles'>Articles</a></li>
                <li class="active">New article</li>
              </Breadcrumb>
            </Col>
          </Row>
          <Row>
            {this.state.loading == true && <Spinner appState={this.state} />}
          </Row>
          {this.state.visible == true &&
            <ArticleForm appState={this.state}/>
          }
        </Col>
      </div>
    )
  }

  defaultState() {
    return {
      id: null,
      title: null,
      body: null,
      category_id: null,
      categories: [],
      visible: false,
      loading: false,
      error: {
        alert_error: false,
        errors: ''
      },
      notification: null,
      formSubmitted: false,
      auth_token: document.cookie.split('rails_react_token_auth_token=')[1] || ''
    }
  }

  constructor(props) {
    super(props)

    this.state = this.defaultState()
  }

  componentDidMount() {
    this.getCategories()
  }

  getCategories() {
    this.setState({
      loading: true
    })

    Api.getCategories().then(response => {
      this.setState({
        categories: response,
        category_id: response[0].id,
        loading: false,
        visible: true
      })
    })
  }
}

export default withRouter(NewArticleComponent)
