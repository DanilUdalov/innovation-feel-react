import React, { Component } from 'react'
import { Row, Col, Label, Well, Breadcrumb, Badge, Alert, ButtonToolbar, DropdownButton, MenuItem, FormControl } from 'react-bootstrap'
import renderHTML from 'react-render-html'

import AlertError from '../shared/AlertError.js'
import Spinner from '../shared/Spinner.js'
import Gallery from 'react-grid-gallery'

const Api = require('../../lib/Api.js')

class ShowArticleContainer extends Component {
  render() {
    return (
      <Col md={10} xs={12}>
        <Row>
          {this.state.loading == true && <Spinner appState={this.state} />}
        </Row>
        <Row>
          {this.state.error.alert_error == true && <AlertError appState={this.state} />}
        </Row>
        {this.state.visible == true &&
          <Well>
            <Row>
              <Col md={9} xs={7}>
                <Breadcrumb>
                  <li><a href='/articles'>Articles</a></li>
                  <li class="active">{this.state.article.title}</li>
                </Breadcrumb>
              </Col>
              <Col md={3} xs={5}>
                <b className='pull-right'>{this.state.article.author}</b>
              </Col>
            </Row>
            <Row>
              <Col md={6} xs={6}>
                <h3>{this.state.article.title}</h3>
              </Col>
              <Col md={6} xs={6}>
                <h3>
                  <Badge className='pull-right'>
                    {this.state.article.category_name}
                  </Badge>
                </h3>
              </Col>
            </Row>
            <Row>
              <Col md={12}>
                {this.state.article.front_image != null && <img className='front_image_show' src={this.state.article.front_image.picture.medium.url} />}
              </Col>
            </Row>
            <br/>
            <Row className='ArticlePage'>
              <Col md={12} xs={12}>
                {renderHTML(this.state.article.body)}
              </Col>
            </Row>
            <Row>
              <Col md={7} xs={6}>
                <Label bsStyle={this.state.status_style}>
                  {this.state.article.status}
                </Label>
              </Col>
              <Col md={5} xs={6}>
                <i className='pull-right'>{this.state.article.posted_date}</i>
              </Col>
            </Row>
            <br/>
            <Row>
              <Gallery
                images={Object.keys(this.state.article.images).map(key => {
                  return {
                    src: this.state.article.images[key].picture.big_large.url,
                    thumbnail: this.state.article.images[key].picture.medium.url,
                    thumbnailHeight: 212,
                    thumbnailCaption: (this.state.role == "moderator" || this.state.role == "admin" ||
                      this.state.article.author_id == this.state.user_id) && (
                      <a
                        id={"images_" + this.state.article.images[key].id}
                        onClick={this.handleFront}
                      >
                        As the front picture
                      </a>
                    )
                  };
                })}
              />
            </Row>
          </Well>
        }
      </Col>
    )
  }

  componentDidMount() {
    this.getArticle()
  }

  setStatusStyle(status_style) {
    if (status_style == 'posted') {
      return 'success'
    } else if (status_style == 'blocked') {
      return 'danger'
    } else {
      return 'warning'
    }
  }

  defaultState() {
    return {
      article: {
        id: this.props.match.params.id,
        title: '',
        body: '',
        category_name: '',
        posted_date: '',
        status: '',
        author: '',
        author_id: '',
        images: '',
        front_image: ''
      },
      auth_token: document.cookie.split('rails_react_token_auth_token=')[1] || '',
      load_action: false,
      role: null,
      user_id: null,
      status_style: '',
      visible: false,
      loading: true,
      notification: '',
      error: {
        alert_error: false,
        errors: ''
      }
    }
  }

  constructor(props) {
    super(props)

    this.state = this.defaultState()
    this.handleFront = this.handleFront.bind(this)
  }

  handleFront(event){
    let auth_token = this.state.auth_token
    let id = event.target.id.split('images_')[1]

    Api.makeFrontImage(auth_token, id).then(response => {
      this.getArticle();
    })
  }

  getArticle() {
    this.setState({ loading: true })

    let article_id = this.props.match.params.id
    let auth_token = this.props.appState.auth_token

    var current_user = JSON.parse(localStorage.getItem("user")) || {}

    Api.getArticle(auth_token, article_id).then(response => {
      if (response.id) {
        this.setState({
          article: {
            title: response.title,
            body: response.body,
            category_name: response.category.name,
            author: response.user.email,
            author_id: response.user.id,
            posted_date: response.posted_date,
            status: response.status,
            images: response.images,
            front_image: response.front_image
          },
          loading: false,
          visible: true,
          status_style: this.setStatusStyle(response.status),
          user_id: current_user.id,
          role: current_user.role
        })
      }
      else {
        this.setState({
          loading: false,
          error: {
            errors: response.error,
            alert_error: true
          }
        })
      }
    })
  }
}

export default ShowArticleContainer
