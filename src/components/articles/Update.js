import React, { Component } from 'react'
import { Row, Col, Breadcrumb } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'

import ArticleForm from './Form.js'
import Spinner from '../shared/Spinner.js'

const Api = require('../../lib/Api.js')
const current_user = JSON.parse(localStorage.getItem('user'))

class UpdateArticleContainer extends Component {
  render() {
    return (
      <div className='updateArticle'>
        <Col md={8} mdOffset={2} xs={12}>
          <Row>
            <Col md={12} xs={12}>
              <Breadcrumb>
                <li><a href='/articles'>Articles</a></li>
                <li class="active">Update article</li>
              </Breadcrumb>
            </Col>
          </Row>
          <Row>
            {this.state.loading == true && <Spinner appState={this.state} />}
          </Row>
          {this.state.visible == true &&
            <ArticleForm appState={this.state} history={this.history} />
          }
        </Col>
      </div>
    )
  }

  defaultState() {
    return {
      id: this.props.match.params.id,
      title: '',
      body: '',
      category_id: '',
      categories: [],
      images: [],
      visible: false,
      loading: false,
      notification: null,
      formSubmitted: false,
      errors: null,
      user_id: null,
      user_role: null,
      auth_token: document.cookie.split('rails_react_token_auth_token=')[1] || null
    }
  }

  constructor(props) {
    super(props)

    this.state = this.defaultState()
  }

  componentDidMount() {
    this.getResources()
  }

  getResources() {
    let article_id = this.state.id
    let auth_token = this.state.auth_token

    this.setState({ loading: true })
    Api.getCategories().then(response => {this.setState({categories: response})})
    Api.getArticle(auth_token, article_id).then(response => {
      if (response.id) {
        if (current_user.id == response.user.id || (current_user.role == 'moderator' || current_user.role == 'admin') ) {
          this.setState({
            title: response.title,
            body: response.body,
            category_id: response.category.id,
            loading: false,
            visible: true,
            images: response.images
          })
        }
        else {
          this.props.articleEditingError(this.props.history)
        }
      }
      else {
        this.setState({
          visible: false,
          errors: response.error
        })
        this.props.history.push('/articles/')
      }
    })
  }
}

export default withRouter(UpdateArticleContainer)
