import React, { Component } from 'react'
import { Alert } from 'react-bootstrap'

class AlertErrorComponent extends Component {
  render() {
    return(
      <Alert bsStyle='danger'>
        <center>
          <h3 className="alert-heading">ERROR</h3>
          <p>{this.props.appState.error.errors}</p>
        </center>
      </Alert>
    )
  }
}

export default AlertErrorComponent
