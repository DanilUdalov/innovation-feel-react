import React, { Component } from 'react'
import { RingLoader } from 'react-spinners'

class SpinnerComponent extends Component {
  render() {
    return(
      <center>
        <div className='sweet-loading'>
          <RingLoader
            color={'#1b9dff'}
            loading={ this.props.appState.loading }
          />
        </div>
      </center>
    )
  }
}

export default SpinnerComponent
