var axios = require('axios')
var hostname = window.location.origin;
var backendHost

if (hostname == 'http://localhost:3000') {
  backendHost = 'http://localhost:3001';
} else if (hostname == 'https://if-blog-test-staging.herokuapp.com') {
  backendHost = 'https://if-blog-article-server-staging.herokuapp.com';
} else if (hostname == 'https://if-blog-test.herokuapp.com') {
  backendHost = 'https://if-blog-article-server.herokuapp.com';
}

module.exports = {
  authenticateUser: function(email, password, remember_me) {
    let data = {
      email: email,
      password: password,
      remember_me: remember_me
    }
    let headers ={ headers: { 'Accept-Version': 'v1' } }

    return axios.post(backendHost + '/api/sessions/log_in', data, headers)
      .then(function (response) {
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  logOutUser: function(auth_token) {
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }

    return axios.delete(backendHost + '/api/sessions/log_out', headers)
  },
  registrationUser: function(email, password, password_confirmation, first_name, surname, username, birthday) {
    let data = {
      email: email,
      password: password,
      password_confirmation: password_confirmation,
      first_name: first_name,
      surname: surname,
      username: username,
      birthday: birthday
    }
    let headers ={
      headers: { 'Accept-Version': 'v1' }
    }
    return axios.post(backendHost + '/api/registrations/sign_up', data, headers)
      .then(function (response) {
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  forgotPassword: function(email) {
    let data = {
      email: email
    }
    let headers ={ headers: { 'Accept-Version': 'v1' } }
    return axios.put(backendHost + '/api/passwords/forgot_password', data, headers)
      .then(function (response) {
        return response.data.message
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  setPassword: function(password, password_confirmation, password_token) {
    let data = {
      password: password,
      password_confirmation: password_confirmation,
      password_reset_token: password_token
    }
    let headers ={ headers: { 'Accept-Version': 'v1' } }
    return axios.put(backendHost + '/api/passwords/' + data.password_reset_token + '/set_new_password', data, headers)
      .then(function(response){
        return response.data.message
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  getArticles: function(auth_token, page, per_page, sort_by, order) {
    let config = {
      headers: { 'Accept-Version': 'v1', 'Auth-Token': auth_token, 'X-Current-Page': page },
      params: {
        page: page,
        per_page: per_page,
        sort_by: sort_by,
        order: order
      },
    }
    return axios.get(backendHost + '/api/articles', config)
      .then(function(response){
        return response
      })
      .catch(function (error) {
        return error.response
      })
  },
  getCategories: function() {
    let headers ={ headers: { 'Accept-Version': 'v1' } }
    return axios.get(backendHost + '/api/categories/category_select', headers).then(function(response) {
      return response.data
    })
  },
  getArticle: function(auth_token, id) {
    auth_token = document.cookie.split('rails_react_token_auth_token=')[1] || ''
    let config = {
      headers: { 'Accept-Version': 'v1', 'Auth-Token': auth_token }
    }
    return axios.get(backendHost + '/api/articles/' + id, config)
      .then(function(response){
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  updateUserRole: function(auth_token, role, id) {
    let data = {
      role: role
    }
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }
    return axios.put(backendHost + '/api/users/' + id + '/update_role', data, headers)
      .then(function(response){
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  updateArticleStatus: function(auth_token, status, id) {
    let data = {
      status: status
    }
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }
    return axios.put(backendHost + '/api/articles/' + id + '/update_status', data, headers)
      .then(function(response){
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  createArticle: function(auth_token, title, body, category_id) {
    let data = {
      article: {
        title: title,
        body: body,
        category_id: category_id
      }
    }
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }
    return axios.post(backendHost + '/api/articles/', data, headers)
      .then(function(response){
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  updateArticle: function(auth_token, id, title, body, category_id) {
    let data = {
      article: {
        title: title,
        body: body,
        category_id: category_id
      }
    }
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }
    return axios.put(backendHost + '/api/articles/' + id, data, headers)
      .then(function(response){
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  getAllArticles: function(auth_token) {
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }
    return axios.get(backendHost + '/api/articles/export_all', headers)
      .then(function(response){
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  getAllComments: function(auth_token) {
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }
    return axios.get(backendHost + '/api/comments/export_all', headers)
      .then(function(response){
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  getAllCategories: function(auth_token) {
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }
    return axios.get(backendHost + '/api/categories/export_all', headers)
      .then(function(response){
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  getAllUsers: function(auth_token) {
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }
    return axios.get(backendHost + '/api/users/export_all', headers)
      .then(function(response){
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  destroyArticle: function(auth_token, id) {
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }

    return axios.delete(backendHost + '/api/articles/' + id, headers)
  },
  createImage: function(auth_token, picture, article_id) {
    const articleFormLoad = new FormData()

    articleFormLoad.append('image[picture]', picture, picture.name)
    articleFormLoad.append('image[article_id]', article_id)

    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }

    return axios.post(`${backendHost}/api/images/`, articleFormLoad, headers).then(function(response){
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  },
  destroyImage: function(auth_token, id) {
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }

    return axios.delete(backendHost + '/api/images/' + id, headers)
  },
  makeFrontImage: function(auth_token, id) {
    let headers ={ headers: { 'Accept-Version': 'v1',  'Auth-Token': auth_token } }
    return axios.put(`${backendHost}/api/images/${id}/make_front`, {}, headers)
      .then(function(response){
        return response.data
      })
      .catch(function (error) {
        return error.response.data
      })
  }
}
